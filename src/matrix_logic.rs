use crate::egui::Response;
use futures_lite::stream::StreamExt;
use ruma::{
    api::client::{filter::FilterDefinition, sync::sync_events},
    assign,
    events::{
        room::message::{MessageType, RoomMessageEventContent, TextMessageEventContent},
        AnySyncMessageLikeEvent, AnySyncTimelineEvent, OriginalSyncMessageLikeEvent,
        SyncMessageLikeEvent,
    },
    presence::PresenceState,
};
use std::time::Duration;

type HttpClient = ruma::client::http_client::Reqwest;
#[doc = r"Logs in the user"]
pub async fn log_messages(
    hose: Response,
    username: Response,
    password: Response,
) -> anyhow::Result<()> {
    let client = ruma::Client::builder()
        .homeserver_url(format!("{:#?}", hose.id))
        .build::<HttpClient>()
        .await?;
    client
        .log_in(
            &String::from(format!("{:#?}", username.id)).to_string(),
            &String::from(format!("{:#?}", password.id)),
            None,
            Some("NetChat"),
        )
        .await?;

    let filter = FilterDefinition::ignore_all().into();
    let initial_sync_response = client
        .send_request(assign!(sync_events::v3::Request::new(), {
            filter: Some(filter),
        }))
        .await?;

    let mut sync_stream = Box::pin(client.sync(
        None,
        initial_sync_response.next_batch,
        PresenceState::Online,
        Some(Duration::from_secs(30)),
    ));

    while let Some(res) = sync_stream.try_next().await? {
        // Only look at rooms the user hasn't left yet
        for (room_id, room) in res.rooms.join {
            for event in room
                .timeline
                .events
                .into_iter()
                .flat_map(|r| r.deserialize())
            {
                // Filter out the text messages
                if let AnySyncTimelineEvent::MessageLike(AnySyncMessageLikeEvent::RoomMessage(
                    SyncMessageLikeEvent::Original(OriginalSyncMessageLikeEvent {
                        content:
                            RoomMessageEventContent {
                                msgtype:
                                    MessageType::Text(TextMessageEventContent {
                                        body: msg_body, ..
                                    }),
                                ..
                            },
                        sender,
                        ..
                    }),
                )) = event
                {
                    println!("{sender}@{room_id}: {msg_body}");
                }
            }
        }
    }

    Ok(())
}
