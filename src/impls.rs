use std::task::Poll;

use eframe::{egui, App};
use serde::{Deserialize, Serialize};
#[derive(Serialize, Deserialize)]
pub struct Arge {
    pub(crate) hs: String,
    pub(crate) user: String,
    pub(crate) pass: String,
}

impl Default for Arge {
    fn default() -> Self {
        Self {
            hs: "matrix.org".to_owned(),
            user: "".to_string(),
            pass: "".to_owned(),
        }
    }
}

impl App for Arge {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, move |ui| {
            ui.heading("NetChat\nA client for the Matrix network");
            ui.vertical_centered(|ui| {
                let hose = ui.label("Homeserver: ");
                let hs_field = ui.text_edit_singleline(&mut self.hs).labelled_by(hose.id);
                let user = ui.label("Username: ");
                let u_feld = ui.text_edit_singleline(&mut self.user).labelled_by(user.id);
                let pass = ui.label("Password: ");
                let p_feld = ui.text_edit_singleline(&mut self.pass).labelled_by(pass.id);

            match ui.button("Log in").clicked() {
                true => {
                    ui.heading("Logging in");
                    Poll::Ready(crate::matrix_logic::log_messages(
                        hs_field, u_feld, p_feld,
                    ))
                }
                false => Poll::Pending,
            }
            });
        });
    }

    fn save(&mut self, _storage: &mut dyn eframe::Storage) {}

    fn on_exit(&mut self, _gl: Option<&eframe::glow::Context>) {}

    fn auto_save_interval(&self) -> std::time::Duration {
        std::time::Duration::from_secs(30)
    }

    fn clear_color(&self, _visuals: &egui::Visuals) -> [f32; 4] {
        egui::Color32::from_rgba_unmultiplied(12, 12, 12, 180).to_normalized_gamma_f32()
    }

    fn persist_egui_memory(&self) -> bool {
        true
    }
}
