#![forbid(unsafe_code, unstable_features, ungated_async_fn_track_caller)]
use std::path;
mod impls;
use eframe::egui;
use impls::Arge;
mod matrix_logic;
fn main() -> anyhow::Result<(), eframe::Error> {
    match path::Path::new(&std::env::var("HOME").unwrap())
        .join(".netchat.toml")
        .exists()
    {
        true => println!("File exists, continuing"),
        false => {
            let f = Arge {
                hs: "".to_string(),
                user: "".to_string(),
                pass: "".to_string(),
            };
            let t = toml::to_string_pretty(&f).unwrap();
            std::fs::write(
                path::Path::new(&std::env::var("HOME").unwrap()).join(".mc.toml"),
                t,
            )
            .unwrap();
        }
    }
    env_logger::init(); // Log to stderr (if you run with `RUST_LOG=debug`).
    let options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default().with_inner_size([320.0, 240.0]),
        ..Default::default()
    };
    eframe::run_native(
        "NetChat",
        options,
        Box::new(|cc| {
            // This gives us image support:
            egui_extras::install_image_loaders(&cc.egui_ctx);

            Box::<Arge>::default()
        }),
    )
}
